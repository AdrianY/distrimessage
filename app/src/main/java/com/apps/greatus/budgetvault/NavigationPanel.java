package com.apps.greatus.budgetvault;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adriany on 2/8/16.
 */
public class NavigationPanel {
    private Map<String, Class<? extends AppCompatActivity>> listedActivities;
    private AppCompatActivity hostActivity;

    private String[] drawerItems;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    public static NavigationPanel generateInstance(final AppCompatActivity hostActivity){
        NavigationPanel instantiatedPanel =  new NavigationPanel(hostActivity);
        instantiatedPanel.initialize();
        return instantiatedPanel;
    }

    private NavigationPanel(final AppCompatActivity hostActivity){
        this.hostActivity = hostActivity;
        mDrawerLayout = (DrawerLayout) hostActivity.findViewById(R.id.drawer_layout);
        drawerItems = hostActivity.getResources().getStringArray(R.array.nav_items);
    }

    void initialize(){
        mTitle = mDrawerTitle = hostActivity.getTitle();
        loadDrawerListItems();
        setupDrawerList();
        setupDrawer();
    }

    private void setupDrawerList(){

        mDrawerList = (ListView) hostActivity.findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        mDrawerList.setAdapter(ArrayAdapter.createFromResource(hostActivity, R.array.nav_items, R.layout.drawer_list_item));

        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                selectItem(position);
            }
        });
    }

    private void setupDrawer(){
        Toolbar toolbar = (Toolbar) hostActivity.findViewById(R.id.tool_bar);
        mDrawerToggle = new ActionBarDrawerToggle(
                hostActivity,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                ActionBar hostActivityToolBar = hostActivity.getSupportActionBar();
                if(null != hostActivityToolBar){
                    hostActivityToolBar.setTitle(mTitle);
                }
            }

            /** Called when a drawer has settled in a completely open state. */
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ActionBar hostActivityToolBar = hostActivity.getSupportActionBar();
                if(null != hostActivityToolBar){
                    hostActivityToolBar.setTitle(mDrawerTitle);
                }
            }

        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void loadDrawerListItems(){
        listedActivities = new HashMap<>();
        listedActivities.put(drawerItems[0], UpdateAccountActivity.class);
        listedActivities.put(drawerItems[1], UpdateSettingsActivity.class);
        listedActivities.put(drawerItems[2], ReadAboutActivity.class);
    }

    /** Swaps fragments in the main content view */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
//        Fragment fragment = new PlanetFragment();
//        Bundle args = new Bundle();
//        args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
//        fragment.setArguments(args);
//
//        // Insert the fragment by replacing any existing fragment
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.content_frame, fragment)
//                .commit();

        // Highlight the selected item, update the title, and close the drawer
        String drawerItemName = drawerItems[position];

        mDrawerList.setItemChecked(position, true);
        setTitle(drawerItemName);
        mDrawerLayout.closeDrawer(mDrawerList);

        Intent intent = new Intent(hostActivity, listedActivities.get(drawerItemName));
        /*String extraMessage = ediText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, extraMessage);*/
        hostActivity.startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setTitle(CharSequence title) {
        mTitle = title;
        ActionBar toolBar = hostActivity.getSupportActionBar();
        if(null != toolBar) {toolBar.setTitle(mTitle);}
    }

    public ActionBarDrawerToggle getmDrawerToggle() {
        return mDrawerToggle;
    }
}
