package com.apps.greatus.budgetvault;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by adriany on 2/6/16.
 */
class MessageActivityListener implements ContactListFragment.OnContactSelectListener, ChatBoxFragment.ChatBoxListener{

    private MessageActivity messageActivity;

    MessageActivityListener(MessageActivity messageActivity){
        this.messageActivity = messageActivity;
    }

    @Override
    public void onProfileSelect(String userId) {

    }

    @Override
    public void onContactSelected(int position) {
        ChatBoxFragment articleFrag = (ChatBoxFragment)
                messageActivity.getSupportFragmentManager().findFragmentById(R.id.chat_box_fragment);

        if (articleFrag != null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the ChatBoxFragment to update its content
//            articleFrag.updateArticleView(position);
        } else {
            // Otherwise, we're in the one-pane layout and must swap frags...

            // Create fragment and give it an argument for the selected article
            ChatBoxFragment newFragment = new ChatBoxFragment();
            Bundle args = new Bundle();
            args.putInt(ChatBoxFragment.ARG_POSITION, position);
            newFragment.setArguments(args);

            FragmentTransaction transaction = messageActivity.getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }
}
